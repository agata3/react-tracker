import Button from "@material-ui/core/Button";

const Button1 = ({ title, onAdd, showAdd }) => {
  return (
    <Button
      onClick={onAdd}
      variant="contained"
      disableElevation
      // color={showAdd ? "secondary" : "primary"}
      style={{ backgroundColor: showAdd ? "#ff1744" : "#43a047" }}
    >
      {title}
    </Button>
  );
};

export default Button1;
