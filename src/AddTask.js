import React from "react";
import TextField from "@material-ui/core/TextField";
import { Box, Checkbox } from "@material-ui/core";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Button from "@material-ui/core/Button";
import { useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import { green } from "@material-ui/core/colors";
const GreenCheckbox = withStyles({
  root: {
    color: green[400],
    "&$checked": {
      color: green[600],
    },
  },
  checked: {},
})((props) => <Checkbox color="default" {...props} />);

const AddTask = ({ onAdd }) => {
  const [text, setText] = useState("");
  const [day, setDay] = useState("");
  const [reminder, setReminder] = useState(false);
  const [name, setName] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!text) {
      alert("Please add a task");
      return;
    }
    onAdd({ text, day, name, reminder });
    setText("");
    setDay("");
    setName("");
    setReminder(false);
  };
  return (
    <form onSubmit={handleSubmit}>
      <Box mt={3} display="flex" flexDirection="column">
        <Box m={0.5}>
          <TextField
            value={name}
            onChange={(e) => setName(e.target.value)}
            fullWidth
            label=" Add Name..."
            variant="outlined"
            style={{ backgroundColor: "#f5f5f5" }}
            type="text"
          />
        </Box>
        <Box m={0.5}>
          <TextField
            value={text}
            onChange={(e) => setText(e.target.value)}
            fullWidth
            label=" Task..."
            variant="outlined"
            style={{ backgroundColor: "#f5f5f5" }}
            type="text"
          />
        </Box>

        <Box m={0.5}>
          <TextField
            value={day}
            onChange={(e) => setDay(e.target.value)}
            fullWidth
            label=" Add Day and Time..."
            variant="outlined"
            style={{ backgroundColor: "#f5f5f5" }}
            type="text"
          />
        </Box>

        <Box my={2}>
          <FormControlLabel
            control={
              <GreenCheckbox
                value={reminder}
                checked={reminder}
                onChange={(e) => setReminder(e.currentTarget.checked)}
                name="checkedA"
              />
            }
            label="Set Reminder"
          />
        </Box>
        <Button variant="contained" color="primary" type="submit">
          Save Task
        </Button>
      </Box>
    </form>
  );
};

export default AddTask;
