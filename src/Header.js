import { Typography, Box } from "@material-ui/core";
import Button1 from "./Button1";

const Header = ({ title, onAdd, showAdd }) => {
  return (
    <Box mb={3} display="flex">
      <Box style={{ width: "100%" }}>
        {" "}
        <Typography color="primary" variant="h4">
          {title}
        </Typography>
      </Box>

      <Box>
        <Button1
          title={showAdd ? "close" : "open"}
          onAdd={onAdd}
          textAlign="right"
          showAdd={showAdd}
        />
      </Box>
    </Box>
  );
};

export default Header;
