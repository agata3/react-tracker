import { createMuiTheme } from "@material-ui/core/styles";

export const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#039be5",
    },
    secondary: {
      main: "#f44336",
    },
    error: {
      main: "#f50057",
    },
  },
  typography: {
    fontFamily: "Comfortaa",
  },
});
