import Header from "./Header";

import Tasks from "./Tasks";
import AddTask from "./AddTask";
import { useState } from "react";

import { Box, Typography } from "@material-ui/core";

const defaultProps = {
  m: 2,
  border: 1,
  p: 3,
};

const App = () => {
  const [showAddTask, setShowAddTask] = useState(false);
  const [tasks, setTask] = useState([
    {
      id: 1,
      name: "Agata",
      text: "Exercise",
      day: "Feb 5th at 2:50",
      reminder: true,
    },
    {
      id: 2,
      name: "Eryk",
      text: "Homework",
      day: "March 21th at 4:00",
      reminder: false,
    },
  ]);

  const addTask = (task) => {
    const id = Math.floor(Math.random() * 9000) + 1;
    const newTask = { id, ...task };
    setTask([...tasks, newTask]);
  };
  const deleteTask = (id) => {
    setTask(tasks.filter((task) => task.id !== id));
  };
  const toggleReminder = (id) => {
    setTask(
      tasks.map((task) =>
        task.id === id ? { ...task, reminder: !task.reminder } : task
      )
    );
  };
  return (
    <Box
      mx={4}
      mt={4}
      borderColor="primary.main"
      {...defaultProps}
      borderRadius={5}
    >
      <Header
        onAdd={() => setShowAddTask(!showAddTask)}
        showAdd={showAddTask}
        title="Task Tracker"
      />

      {tasks.length > 0 ? (
        <Tasks tasks={tasks} onDelete={deleteTask} onToggle={toggleReminder} />
      ) : (
        <Typography>No Tasks to Show</Typography>
      )}

      {showAddTask && <AddTask onAdd={addTask} />}
    </Box>
  );
};

export default App;
