import React from "react";
import ClearIcon from "@material-ui/icons/Clear";
import Paper from "@material-ui/core/Paper";
import { Box, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
const useStyles = makeStyles({
  root: {
    borderLeft: "5px solid #66bb6a",
  },
});

const Task = ({ onDelete, task, onToggle }) => {
  const classes = useStyles();

  return (
    <Box className={task.reminder ? classes.root : " "}>
      <Paper style={{ backgroundColor: "#eeeeee" }}>
        <Box m={1} alignItems="center" display="flex">
          <Box onClick={() => onToggle(task.id)}>
            <Typography>{task.name}</Typography>
          </Box>
          <Box m={1} onClick={() => onToggle(task.id)}>
            <Typography> {task.text}</Typography>
          </Box>
          <Box
            style={{ width: "100%" }}
            m={1}
            onClick={() => onToggle(task.id)}
          >
            <Typography> {task.day}</Typography>
          </Box>

          <Box>
            <ClearIcon
              color="secondary"
              onClick={() => onDelete(task.id)}
            ></ClearIcon>
          </Box>
        </Box>
      </Paper>
    </Box>
  );
};

export default Task;

// {/* <Tabs

//           indicatorColor="primary"
//           textColor="primary"
//           className={classes.root}
//         >
//           <Typography variant="h6">
//             <Tab label={task.name} />
//             <Tab label={task.text} />
//             <ClearIcon
//               color="error"
//               onClick={() => onDelete(task.id)}
//             ></ClearIcon>
//           </Typography>
//         </Tabs> */}
